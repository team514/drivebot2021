// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.CollectorUtil;

public class ShotCommand extends CommandBase {

  private CollectorUtil collectorUtil;

  /** Creates a new ShotCommand. */
  public ShotCommand(CollectorUtil collectorUtil) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.collectorUtil = collectorUtil;
    addRequirements(collectorUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    collectorUtil.deployPiston();
    collectorUtil.runBelt(0.5);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    collectorUtil.retractPiston();
    collectorUtil.runBelt(0.0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
